package homeWorks.homeWork8;

public class Main {
    public static void main(String[] args) {
        FlowerStore store = new FlowerStore();

        System.out.println("Method sell : ");
        int summ = 0;
        for (Flower f : store.sell(6, 1, 2)) {
            System.out.println(f.getClass().getSimpleName());
            summ += f.getPrice();
            store.setPocket(store.getPocket() + f.getPrice());
        }

        System.out.println("TOTAL = " + summ);
        System.out.println(store.getPocket());

        System.out.println("\nMethod sellSequence : ");
        for (Flower f : store.sellSequence(6, 5, 4)) {
            System.out.println(f.getClass().getSimpleName());
        }

        String path = "C:\\torro\\bora.txt";

        FlowersSaver.save(path, store.sell(6, 1, 2));

        System.out.println("\nFrom file :\n");
        for (Flower f : FlowersLoader.load(path)) {
            System.out.println(f.getClass().getSimpleName());
        }
    }
}
