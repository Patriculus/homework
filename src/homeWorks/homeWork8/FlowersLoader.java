package homeWorks.homeWork8;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

abstract class FlowersLoader {
    public static Flower[] load(String path) {

        ArrayList<Flower> list = new ArrayList<>();
        try {
            FileInputStream fstream = new FileInputStream(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                list.add(createFlowerObject(strLine));
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        Flower[] bouquet = new Flower[list.size()];
        for (int i = 0; i < list.size(); i++) {
            bouquet[i] = list.get(i);
        }
        return bouquet;
    }

    private static Flower createFlowerObject(String strLine) {
        if (strLine.equals("Rose")) {
            return new Rose();
        } else if (strLine.equals("Chamomile")) {
            return new Chamomile();
        } else {
            return new Tulip();
        }
    }
}
