package homeWorks.homeWork8;

public class FlowerStore {
    private int pocket;
    Flower[] bouquet;

    public Flower[] sell(int roses, int chamomiles, int tulips) {
        int flowerCount = roses + chamomiles + tulips;
        bouquet = new Flower[flowerCount];
        for (int i = 0; i < flowerCount; i++) {
            if (i < roses) {
                bouquet[i] = new Rose();
            }
            if (i >= roses && i < (roses + chamomiles)) {
                bouquet[i] = new Chamomile();
            }
            if (i >= roses + chamomiles) {
                bouquet[i] = new Tulip();
            }
        }
        return bouquet;
    }

    public Flower[] sellSequence(int roses, int chamomiles, int tulips) {
        int maxFlowersType = biggest(roses, chamomiles, tulips);
        bouquet = new Flower[roses + chamomiles + tulips];
        int flowersCounter = 0;
        for (int i = 0; i < maxFlowersType; i++) {
            if (roses != 0) {
                bouquet[flowersCounter++] = new Rose();
                roses--;
            }

            if (chamomiles != 0) {
                bouquet[flowersCounter++] = new Chamomile();
                chamomiles--;
            }

            if (tulips != 0) {
                bouquet[flowersCounter++] = new Tulip();
                tulips--;
            }
        }

        return bouquet;
    }

    private int biggest(int a, int b, int c) {
        return a > b ? (a > c ? a : c) : (b > c ? b : c);
    }

    public int getPocket() {
        return pocket;
    }

    public void setPocket(int pocket) {
        this.pocket = pocket;
    }
}
