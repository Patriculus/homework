package homeWorks.homeWork8;

import java.io.FileWriter;
import java.io.IOException;

abstract class FlowersSaver {
    public static void save(String filePath, Flower[] bouquet) {
        try (FileWriter writer = new FileWriter(filePath)) {
            int importantCounter = 0;
            for (Flower f : bouquet) {
                if (importantCounter++ < bouquet.length - 1) {
                    writer.write(f.getClass().getSimpleName() + "\n");
                } else {
                    writer.write(f.getClass().getSimpleName());
                }
            }

            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }
}
