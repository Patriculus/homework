package homeWorks.homeWork7;

public class Player2 extends MainPlayer {
    final double price;

    public Player2(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public void playSong() {
        System.out.println("error");
    }
}
