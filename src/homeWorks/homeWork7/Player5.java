package homeWorks.homeWork7;

public class Player5 extends PlayersWithPlaylist {
    final double price;

    public Player5(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public void playAllSongs() {
        for (int i = getPlayList().length - 1; i >= 0; i--) {
            System.out.println("Playing: " + getPlayList()[i]);
        }
    }
}
