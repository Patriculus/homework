package homeWorks.homeWork7;

public class MainPlayer {
    private String song;

    public void playSong(){
        System.out.println("Playing: " + song);
    }

    public void playSong(String s){
        System.out.println("Playing: " + s);
    }

    public void setSong(String song) {
        this.song = song;
    }
}
