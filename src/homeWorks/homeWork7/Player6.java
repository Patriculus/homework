package homeWorks.homeWork7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Player6 extends PlayersWithPlaylist {
    final double price;

    public Player6(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void shuffle() {

        ArrayList<String> list = new ArrayList(Arrays.asList(getPlayList()));

        Collections.shuffle(list);

        String[] newPlayList = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            newPlayList[i] = list.get(i);
        }

        setPlayList(newPlayList);
    }
}
