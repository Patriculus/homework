package homeWorks.homeWork7;

public class Player4 extends PlayersWithPlaylist {
    final double price;

    public Player4(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public void playSong() {
        System.out.println("Playing: " + getPlayList()[getPlayList().length - 1]);
    }
}
