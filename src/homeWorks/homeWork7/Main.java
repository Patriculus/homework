package homeWorks.homeWork7;

public class Main {
    public static void main(String[] args) {
        String song = "BLA BLA BLA";
        String[] playList = {"Song1", "Song2", "Song3", "Song4", "Song5"};

        System.out.println("PLAYER 1 :");
        Player1 player1 = new Player1(12);
        player1.setSong(song);
        player1.playSong();

        System.out.println(" \nPLAYER 2 :");
        Player2 player2 = new Player2(15);
        player2.setSong(song);
        player2.playSong();

        System.out.println(" \nPLAYER 3 :");
        Player3 player3 = new Player3(18);
        player3.setPlayList(playList);
        player3.playSong();
        player3.playAllSongs();

        System.out.println(" \nPLAYER 4 :");
        Player4 player4 = new Player4(21);
        player4.setPlayList(playList);
        player4.playSong();
        player4.playAllSongs();

        System.out.println(" \nPLAYER 5 :");
        Player5 player5 = new Player5(24);
        player5.setPlayList(playList);
        player5.playSong();
        player5.playAllSongs();

        System.out.println(" \nPLAYER 6 :");
        Player6 player6 = new Player6(27);
        player6.setPlayList(playList);
        player6.playSong();
        player6.playAllSongs();
        player6.shuffle();
        System.out.println("\nShuffled \n");
        player6.playAllSongs();
    }
}
