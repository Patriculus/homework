package homeWorks.homeWork7;

public class PlayersWithPlaylist extends MainPlayer{
    private String[] playList;

    public void playAllSongs(){
        for(String s : playList){
            playSong(s);
        }
    }

    @Override
    public void playSong() {
        System.out.println("Playing: " + playList[0]);
    }

    public void setPlayList(String[] playList) {
        this.playList = playList;
    }

    public String[] getPlayList() {
        return playList;
    }
}
