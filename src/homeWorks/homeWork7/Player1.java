package homeWorks.homeWork7;

public class Player1 extends MainPlayer {
    final double price;

    public Player1(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
