package homeWorks.homeWork7;

public class Player3 extends PlayersWithPlaylist {
    final double price;

    public Player3(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}
