package homeWorks.homeWork10;

public enum FruitsType {
    APPLE,
    BANANA,
    STRAWBERRY,
    PEAR,
    PLUM,
    RASPBERRIES,
    GRAPES
}
