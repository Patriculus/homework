package homeWorks.homeWork2;

//HEX converter
public class Work1b {
    public static void main(String[] args) {
//        int a = (int) (Math.random() * 1000);
        int a = 255;
        System.out.println("Number is " + a);
        String result = "";
        if(!(a>16 && a<32)){
            while (a / 16 >9 || a == 16) {
                int b = a % 16;
                result = numberConvertor(b) + result;
                a = a / 16;
            }}else
        {
            result = a % 16 + result;
            a = a / 16;
        }
        result = numberConvertor(a) + result;
        System.out.println("result = " + result);
    }

    public static String numberConvertor(int numb) {
        String res = "";

        switch (numb) {
            case 10:
                res = "a";
                break;
            case 11:
                res = "b";
                break;
            case 12:
                res = "c";
                break;
            case 13:
                res = "d";
                break;
            case 14:
                res = "e";
                break;
            case 15:
                res = "f";
                break;
            default:
                res = "" + numb;
                break;

        }

        return res;
    }
}
