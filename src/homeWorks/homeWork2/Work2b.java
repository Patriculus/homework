package homeWorks.homeWork2;
//Сортировка перемешиванием
public class Work2b {
    public static void main(String[] args) {
        int[] array = new int[5];

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 100);
        }

        showMassive(array);

        int buff;
        int left=0;
        int right=array.length-1;
        do {
            for (int i=left; i<right;i++) {
                if (array[i]>array[i+1]) {
                    buff = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = buff;
                }
            }
            right--;
            for (int i=right; i>left; i--) {
                if (array[i]<array[i-1]) {
                    buff = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = buff;
                }
            }
            left++;
        } while (left <right);

        System.out.println("Sorted");
        showMassive(array);
    }

    public static void showMassive(int[] mass) {
        for (int i : mass) {
            System.out.println(i);
        }
    }
}
