package homeWorks.homeWork2;

public class Work4 {
    public static void main(String[] args) {
//        int rnd = (int) (Math.random() * 100) + 1;
        int rnd = 7;
        int[] mass = new int[rnd];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = (int) (Math.random() * 100);
        }

        System.out.println("Generated massive :");
        showMassive(mass);
        for (int i = 0; i < mass.length - 1; i++) {
            for(int j =0; j<mass.length -1 -i; j++) {
                if (mass[j] > mass[j + 1]) {
                    int buff = mass[j];
                    mass[j] = mass[j + 1];
                    mass[j + 1] = buff;
                }
            }
        }
        System.out.println("Sorting finished");

        showMassive(mass);
        System.out.println("\n Mediana is : ");
        if(mass.length%2 != 0){
            System.out.print(mass[mass.length/2]);
        }else {
            System.out.print((mass[(mass.length/2)-1]+ mass[mass.length/2])/2d);
        }

    }

    public static void showMassive(int [] mass){
        for (int i : mass) {
            System.out.println(i);
        }
    }
}
