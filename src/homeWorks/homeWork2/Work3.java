package homeWorks.homeWork2;

//3)Создайте массив из 20-ти первых чисел Фибоначчи и выведите его на экран.
// Напоминаем, что первый и второй члены последовательности равны единицам,
// а каждый следующий — сумме двух предыдущих.


public class Work3 {
    public static void main(String[] args) {
        int[] mass = new int[20];
        mass[0] = 0;
        mass[1] = 1;
        for (int i = 2; i < 20; i++) {
            mass[i] = mass[i - 1] + mass[i - 2];
        }

        for(int i : mass) System.out.println(i);
    }
}
