package homeWorks.homeWork2;

//Сортировка вставками
public class Work2a {
    public static void main(String[] args) {
        int[] arr = new int[5];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        showMassive(arr);

        int temp, j;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                temp = arr[i + 1];
                arr[i + 1] = arr[i];
                j = i;
                while (j > 0 && temp < arr[j - 1]) {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
        }
        System.out.println("Sorted");
        showMassive(arr);
    }

    public static void showMassive(int[] mass) {
        for (int i : mass) {
            System.out.println(i);
        }
    }
}
