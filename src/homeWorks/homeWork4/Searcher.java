package homeWorks.homeWork4;

import java.io.File;

public class Searcher {
    public static void main(String[] args) {
        String lookingFor = "patryck.txt";
        final String FOLDER_NAME = "C:\\torro";
        File folder = new File(FOLDER_NAME);

        String result = findFile(lookingFor, folder);
        if ("".equals(result)) {
            System.out.println("File was not found");
        } else {
            System.out.println("File path is : " + result);
        }
    }

    private static String findFile(String lookingFor, File folder) {
        String found = "";
        for (File f : folder.listFiles()) {
            if (f.isDirectory()) {
                found = findFile(lookingFor, f);
            } else {
                if (lookingFor.equals(f.getName())) {
                    found = f.getAbsolutePath();
                    break;
                }
            }
        }
        return found;
    }
}
