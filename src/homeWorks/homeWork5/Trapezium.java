public class Trapezium { //получаю 4 отрезка для создания и счета площади трапеции возвращаю площадь

    private Point near1, near2, further1, further2;
    private double area;

    public double getArea() {
        return area;
    }


    public Trapezium(Point near1, Point near2, Point further1, Point further2) {
        this.near1 = near1;
        this.near2 = near2;
        this.further1 = further1;
        this.further2 = further2;
        calculedArea(near1, near2, further1, further2);
    }

    private void calculedArea(Point near1, Point near2, Point further1, Point further2) {
        double sideA, sideB, sideC, sideD;
        sideA = Utils.getDist(near1, near2);
        sideB = Utils.getDist(near2, further1);
        sideC = Utils.getDist(further1, further2);
        sideD = Utils.getDist(further2, near1);
        double semiPerimeter = (sideA + sideB + sideC + sideD) / 2;
        area = Math.sqrt((semiPerimeter - sideA) * (semiPerimeter - sideB) * (semiPerimeter - sideC) * (semiPerimeter - sideD));
    }


}
