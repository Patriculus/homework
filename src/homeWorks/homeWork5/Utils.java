import java.util.List;

public class Utils {
    public static Line createLine(Point p1, Point p2) {
        double a, b, c;
        a = p1.getY() - p2.getY();
        b = p2.getX() - p1.getX();
        c = (p1.getX() * p2.getY()) - (p2.getX() * p1.getY());
        if (a == 0 && b == 0) {
            return null;
        }
        return new Line(a, b, c);
    }

    static double getDist(Point p1, Point p2) {
        return Math.abs(Math.sqrt((p2.getX() - p1.getX()) * (p2.getX() - p1.getX()) + (p2.getY() - p1.getY()) * (p2.getY() - p1.getY())));
    }

    static Line[] generateLine(Point[] points) {
        Line[] lines = new Line[points.length];
        for (int i = 0; i < points.length - 1; i++) {
            lines[i] = createLine(points[i], points[i + 1]);
        }
        lines[points.length - 1] = createLine(points[points.length - 1], points[0]);
        return lines;
    }

    public static boolean canBuild(Point[] points) {
        boolean direction;
        Point vector1, vector2;

        vector1 = Utils.getVector(points[0], points[points.length - 1]);
        vector2 = Utils.getVector(points[0], points[1]);
        direction = getDirectionVectorSing(vector1, vector2);

        for (int i = 1; i < points.length - 1; i++) {
            vector1 = Utils.getVector(points[i], points[i - 1]);
            vector2 = Utils.getVector(points[i], points[i + 1]);
            if (direction != getDirectionVectorSing(vector1, vector2)) {
                return false;
            }
        }
        vector1 = Utils.getVector(points[points.length - 1], points[points.length - 2]);
        vector2 = Utils.getVector(points[points.length - 1], points[0]);
        return direction == getDirectionVectorSing(vector1, vector2);
    }

    private static Point getVector(Point p1, Point p2) {
        return new Point(p2.getX() - p1.getX(), p2.getY() - p1.getY());
    }

    private static boolean getDirectionVectorSing(Point p1, Point p2) {
        return (p1.getX() * p2.getY() - p1.getY() * p2.getX()) > 0;
    }

    private static Point getNeatCrossPoint(Point[] points0, Point maxPoint, Point observer, Point totalMin, Point strikePoint) {
        Point[] points = new Point[points0.length + 1];
        System.arraycopy(points0, 0, points, 0, points0.length);
        points[points0.length] = points[0];
        Line[] lines = Utils.generateLine(points0);
        Line maxDistLine = Utils.createLine(maxPoint, observer);
        double maxDist = Utils.getDist(maxPoint, observer);
        double totalMinDist = Utils.getDist(totalMin, observer);
        if (maxDistLine == null) {
            return null;
        }
        Point crossPoint = maxPoint;
        Point bestCrossPoint = maxPoint;
        double distance, bestCrossPointDist = maxDist;
        for (int i = 0; i < points.length - 1; i++) {
            if (lines[i] != null) {
                crossPoint = Utils.getCrossPoint(lines[i], maxDistLine);
            }
            distance = Utils.getDist(observer, crossPoint);
            if (distance < maxDist) {
                if (distance <= bestCrossPointDist) {
                    if (strikePoint != null && Utils.same(crossPoint, strikePoint)) {
                        bestCrossPoint = crossPoint;
                        bestCrossPointDist = distance;
                    }
                }
            }
        }
        return bestCrossPoint;
    }

    private static Point getFurtherCrossPoint(Point[] points0, Point minPoint, Point observer, Point totalMax, Point strikePoint) {
        Point[] points = new Point[points0.length + 1];
        System.arraycopy(points0, 0, points, 0, points0.length);
        points[points0.length] = points[0];
        Line[] lines = Utils.generateLine(points0);
        Line minDistLine = Utils.createLine(observer, minPoint);
        double minDist = Utils.getDist(minPoint, observer);
        double totalMaxDist = Utils.getDist(totalMax, observer);
        if (minDistLine == null) {
            return null;
        }
        Point crossPoint, bestCrossPoint = minPoint;
        double dist, bestCrossPointDist = minDist;
        for (int i = 0; i < points.length - 1; i++) {
            if (lines[i] != null) {
                crossPoint = Utils.getCrossPoint(lines[i], minDistLine);
                dist = Utils.getDist(observer, crossPoint);
                if (dist > minDist && dist <= totalMaxDist) {
                    if (dist >= bestCrossPointDist) {
                        if (strikePoint != null && Utils.same(crossPoint, strikePoint)) {
                            continue;
                        }
                        bestCrossPoint = crossPoint;
                        bestCrossPointDist = dist;
                    }
                }
            }
        }

        return bestCrossPoint;
    }

    public static Point getCrossPoint(Line l1, Line l2) {
        final Double a1, a2, b1, b2, c1, c2, x, y;
        a1 = l1.getA();
        b1 = l1.getB();
        c1 = l1.getC();

        a2 = l2.getA();
        b2 = l2.getB();
        c2 = l2.getC();

        y = (((a2 / a1) * c1) - c2) / (b2 - ((a2 / a1) * b1));
        x = (((b1 * y) + c1) / a1) * (-1);

      /*  if (x.isNaN() || y.isNaN()) {
            x = (((b2 / b1) * c1) - c2) / (a2 - (b2 / b1) * a1);
            y = (((a1 * x) + c1) / b1) * (-1);
        }*/

        return new Point(x, y);
    }

    public static boolean same(double num1, double num2) {
        return Math.abs(num1 - num2) < 0.001;
    }

    public static boolean same(Point p1, Point p2) {
        return same(p1.getX(), p2.getX()) && same(p1.getY(), p2.getY());
    }

    private static List<Trapezium> getTrapeziun(Point[] points, Point observer) {
        return null;
    }

    public static boolean isPointBelogToStretch(Point begin, Point end, Point point) {
        if (begin.getX() >= end.getX()) {
            if (begin.getY() >= end.getY()) {
                return begin.getX() >= point.getX() && point.getX()
            }
        }
    }

}

