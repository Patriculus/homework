import classWorks.Rectangle.Point;
import com.google.api.client.googleapis.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
    public static void main(String[] args) {
        List<Point> listPoints = new ArrayList<>();
        listPoints.add(new Point(10, 1));
        listPoints.add(new Point(11, 2));
        listPoints.add(new Point(14, 7));
        listPoints.add(new Point(14, 9));
        listPoints.add(new Point(11, 10));
        listPoints.add(new Point(5, 8));
        listPoints.add(new Point(4, 3));
        listPoints.add(new Point(5, 2));

        Point[] pointsArray = listPoints.toArray(new Point[0]);

        System.out.println(Utils.canBuild(pointsArray));

        Point observer = new Point(32,12);



    }


}
