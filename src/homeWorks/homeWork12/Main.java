package homeWorks.homeWork12;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        int            peopleCount;
        int            maxPeople;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter people count");
        peopleCount = Integer.parseInt(reader.readLine());

        System.out.println("Enter max people amount in library");
        maxPeople = Integer.parseInt(reader.readLine());

        Library library = new Library(maxPeople);

        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            String name = String.format("human_%d", i);
            library.setHuman(new Thread(new Human(name)));
        }

    }
}
