package homeWorks.homeWork12;

import java.util.ArrayList;
import java.util.List;

public class Library implements Runnable{
    private int maxAmount;
    private int peopleCount;

    private List<Thread> listHumans = new ArrayList<>();

    public Library(int maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void setHuman(Thread human) {
        while (listHumans.size() <= maxAmount) {
            this.listHumans.add(human);
            human.run();
        }
    }

    @Override
    public void run() {
        while (true) {
            for(Thread t : listHumans){
                if(!t.isAlive()){
                    t.interrupt();
                    listHumans.remove(t);
                }
            }
        }
    }
}
