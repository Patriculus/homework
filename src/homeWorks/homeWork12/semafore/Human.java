package homeWorks.homeWork12.semafore;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Human extends Thread{
    Semaphore enterToLibrary;

    public void run(){
        System.out.println(this.getName() + " пришел ко входу в библиотеку");
        try{
            enterToLibrary.acquire();
            System.out.println(this.getName() + " подошел к двери с улицы");
            System.out.println(this.getName() + " ждет входа в библиотеку");

            System.out.println(this.getName() + " вошел в библиотеку");
            System.out.println(this.getName() +  " читает книгу");
            int waitTime = new Random().nextInt(2000)+500;
            sleep(waitTime);

            System.out.println(this.getName() + " вышел из библиотеки");
            enterToLibrary.release();
        }catch (Exception e){
            System.out.println(e);
        }
    }

}
