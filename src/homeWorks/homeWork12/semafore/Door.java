package homeWorks.homeWork12.semafore;

import java.util.Random;
import java.util.concurrent.Semaphore;

public class Door extends Thread{
    Semaphore goThruDoor;

    public void run(){
        try{
            System.out.println(this.getName() + " подошел к двери с улицы");
            goThruDoor.acquire();
            System.out.println(this.getName() + " проходит через дверь внутрь");
            System.out.println(this.getName() + " прошел через дверь внутрь");
            System.out.println(this.getName() +  " читает книгу");
            int waitTime = new Random().nextInt(2000)+500;
            sleep(waitTime);
            System.out.println(this.getName() + " вышел из библиотеки");
            goThruDoor.release();
        }catch (Exception e){
            System.out.println(e);
        }
    }

}
