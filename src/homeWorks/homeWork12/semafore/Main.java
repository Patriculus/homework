package homeWorks.homeWork12.semafore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) throws IOException {
        Semaphore library;
        int maxCount;
        int peopleCount;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter max count people in library");
        maxCount = Integer.parseInt(reader.readLine());

        System.out.println("Enter people count want enter to library");
        peopleCount = Integer.parseInt(reader.readLine());

        library = new Semaphore(maxCount);

        List<Human> list = new ArrayList<>();

        for(int i = 0; i< peopleCount; i++){
            list.add(new Human());
        }

        for(Human h : list){
            h.enterToLibrary = library;
            h.start();
        }

    }
}
