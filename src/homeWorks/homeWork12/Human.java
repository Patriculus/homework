package homeWorks.homeWork12;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Human implements Runnable{
    private String humanName;
    private List<String> commands;

    public Human(String name) {
        this.humanName = name;
        commands = new ArrayList<>();
        commands.add("пришел ко входу в библиотеку");
        commands.add("ждет входа в библиотеку");
        commands.add("вошел в библиотеку");
        commands.add("читает книгу");
        commands.add("вышел из библиотеки");
    }

    public String gethumanName() {
        return humanName;
    }

    public List<String> getCommands() {
        return commands;
    }

    @Override
    public void run() {
        for(String s : commands){
            if(s.equals("читает книгу")){
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(humanName + " " + s);
        }
    }
}
