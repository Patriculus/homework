package homeWorks.homeWork1;

public class Work4 {
    public static void main(String[] args) {
        int rndNumber = 5 + (int) (Math.random() * 150);
        if (rndNumber > 25 && rndNumber < 100) {
            System.out.println("Число " + rndNumber + " содержится в интервале (25,100)");
        } else {
            System.out.println("Число " + rndNumber + " не содержится в интервале (25,100)");
        }
    }
}
