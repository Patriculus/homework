package homeWorks.homeWork1;

public class Work1 {
    public static void main(String[] args) {
        int rnd = (int) (Math.random() * 100) + 1;
        System.out.println("rnd = " + rnd);
        if (rnd <= 70) {
            if (rnd > 0 && rnd <= 20) {
                System.out.println(rnd % 8);
            }
            if (rnd >= 15 && rnd < 45) {
                System.out.println(rnd % 10);
            }
            if (rnd > 45) {
                System.out.println(rnd / (5 / 8d));
            }
        } else {
            System.out.println(rnd / 5d);
        }
    }
}
