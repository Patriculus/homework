package classWorks.classWork17;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.1.208", 7035);
//        Socket socket = new Socket("192.168.89.250", 7035);
//

        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

        dataOutputStream.writeUTF("Hello1");
        dataOutputStream.flush();

        System.out.println(dataInputStream.readUTF());


        dataOutputStream.close();
        dataInputStream.close();

        socket.close();
    }

}