package classWorks.classWork6;

public class Cat extends Animal implements Noiser{
    public Cat() {
        super(4, "KOT", 12);
    }

    @Override
    public void setPawCount(int pawCount) {
        if(pawCount>4) {
            super.setPawCount(4);
            return;
        }
        super.setPawCount(pawCount);
    }

    @Override
    public void getVoice() {

    }

        @Override
        public String toNoise() {
            return "Шкрбусь в стену";
        }

    public String killCat(){
        return "Cat is dead";
    }
}

