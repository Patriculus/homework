package classWorks.classWork6;


public abstract class Animal {
    public Animal(int age, String name, int pawCount) {
        this.age = age;
        this.name = name;
        this.pawCount = pawCount;
    }

    public Animal(int age, int pawCount) {
        this.age = age;
        this.pawCount = pawCount;
        name = "";
    }

    public Animal() {
        name = "Boris";
    }

    private int age;
    private String name;

    public void setAge(int age) {
        if(age<0){
            this.age = 0;
        }else {
            this.age = age;
        }
    }

    public void setPawCount(int pawCount) {
        if(pawCount<0){
            this.pawCount = 0;
        }else {
            this.pawCount = pawCount;
        }

    }

    private int pawCount;

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public int getPawCount() {
        return pawCount;
    }

    public String getType(){
        return this.getClass().getSimpleName();
    }

    public abstract void getVoice();

    public String toString(){
        return "666";
    }

}
