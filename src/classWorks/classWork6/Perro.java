package classWorks.classWork6;

public class Perro extends Animal implements Noiser{

    @Override
    public void setPawCount(int pawCount) {
        if (pawCount > 2) {
            super.setPawCount(2);
            return;
        }
        super.setPawCount(pawCount);
    }

    @Override
    public void getVoice() {

    }

    @Override
    public String toNoise() {
        return null;
    }
}
