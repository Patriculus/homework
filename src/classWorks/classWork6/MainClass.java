package classWorks.classWork6;

public class MainClass {
    public static void main(String[] args) {
        Cat cat = new Cat();
        Perro perro = new Perro();
//        Animal cat2 = new Cat();

        Animal [] mass = new Animal[5];

        mass[0] = new Perro();
        mass[1] = new Cat();
        mass[2] = new Perro();
        mass[3] = new Cat();
        mass[4] = new Perro();

        for(Animal animal1222 : mass){
//            if(animal1222.getType().equals(Cat.class.getSimpleName())){
            if(animal1222 instanceof Cat){
//            System.out.println((animal1222 instanceof Cat ? ((Cat) animal1222) : null).killCat());
                System.out.println(((Cat)animal1222).killCat());
            }
//            System.out.println(animal1222.getType());
        }

        Noiser [] mass1 = new Noiser[3];
        mass1[0] = new Cat();
        mass1[1] = new Perro();
        mass1[2] = new ScrewDriver();



        for(Noiser n: mass1){
            System.out.println(n.toNoise());
        }

//        cat.setPawCount(-1);
//        animal.setPawCount(-1);

        perro.setPawCount(3);
        cat.setPawCount(5);

        System.out.println(perro.getPawCount());
        System.out.println(cat.getPawCount());

        System.out.println(new IGetterInterface() {
            @Override
            public String getSring() {
                return "I'm mr Grey";
            }
        }.getSring());

        System.out.println(new Cat().toString());
    }
}
