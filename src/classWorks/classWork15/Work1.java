package classWorks.classWork15;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.concurrent.locks.ReentrantLock;

public class Work1 {
    private static List<Integer> list = new ArrayList<>();

    private static Timer timer = new Timer();
    private static ReentrantLock flag = new ReentrantLock();

    public static void main(String[] args) {

//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                flag.lock();
//                System.out.println(list);
//                list = new ArrayList<>();
//                flag.unlock();
//                startThread();
//            }
//        }, 0, 1);


        Thread thread = new Work2();
        thread.start();
        thread.stop();

    }

    private static void startThread() {
        new Thread() {
            @Override
            public void run() {
                setFlag(this, 30);
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                setFlag(this, 40);
            }
        }.start();
    }


    private static void setFlag(Thread thread, Integer n) {
//        while (true) {
        flag.lock();
        list.add(n);
        flag.unlock();


//            if (flag.isLocked()) {
//                flag = true;
//                list.add(n);
//                flag = false;
//                return;
//            } else {
//                try {
//                    thread.sleep(4);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }

}