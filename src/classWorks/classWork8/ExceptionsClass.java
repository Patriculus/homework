package classWorks.classWork8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExceptionsClass {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            readInt(reader);
            throw new MyException("GO GO GO");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e1) {
            System.out.println(e1.getString());
        }


        System.out.println("LOL");
    }

    private static int readInt(BufferedReader reader) throws IOException {
        Integer i = null;
        try {
            i = Integer.parseInt(reader.readLine());
            return i;
        } catch (NumberFormatException e1){
           return readInt(reader);
        }finally {

        }

    }
}


