package classWorks.classWork8;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class MainClass {
    public static void main(String[] args) {
        Car aClass = new Car();

        for (Field item : aClass.getClass().getDeclaredFields()) {
            System.out.println(item.getName());
            for (Annotation item1 : item.getAnnotations()) {
                if (item1.annotationType().equals(MyAnnotat.class)) {
                    System.out.println(((MyAnnotat) item1).partOfCar());
                }

                //System.out.println(((LinkedHashMap) ((AnnotationInvocationHandler) ((Proxy) item1).h).memberValues).values());
//                System.out.println("");
//                System.out.println(item1.annotationType().getName());
            }
//            System.out.println(item.getName());
        }
    }
}
