package classWorks.classWork8;

public class MyException extends Exception{
    private String string;
    public MyException(String s){
        this.string = s;
    }

    public String getString() {
        return string;
    }
}
