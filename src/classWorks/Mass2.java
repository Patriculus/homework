package classWorks;

import java.util.Random;

public class Mass2 {
    public static void main(String[] args) {
        int [] mass = {4, -5, 0, 6, 8};

        int min = mass[0];
        int minPoss = 0;
        int max = min;
        int maxPoss = 0;

        for(int i = 1; i<mass.length; i++){
            if(mass[i]<min){
                min = mass[i];
                minPoss = i;
            }

            if(mass[i]>max){
                max = mass[i];
                maxPoss = i;
            }
        }

        int buff = mass[minPoss];
        mass[minPoss] = mass[maxPoss];
        mass[maxPoss] = buff;

        for(int i: mass){
            System.out.println(i);
        }
    }
}
