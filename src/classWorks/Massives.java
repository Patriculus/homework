package classWorks;

public class Massives {
    public static void main(String[] args) {
        int[] mass = new int[10];
        int iter = 0;
        for (int i = 0; i < 20; i++) {
            if (i % 2 != 0) {
                mass[iter] = i;
                iter++;
            }
        }

        for (int b = 0; b < mass.length; b++) {
            if (b == mass.length - 1) {
                System.out.print(mass[b]);
            } else {
                System.out.print(mass[b] + ", ");
            }
        }

    }
}
