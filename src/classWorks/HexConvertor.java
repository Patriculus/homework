package classWorks;

public class HexConvertor {
    public static void main(String[] args) {
//        int a = (int) (Math.random() * 1000);
        int a = 545;
        System.out.println("Number in OCT metric system is " + a);
        String result = "";
        while (a / 8 >= 7 || a == 8) {
            int b = a % 8;
            result = b + result;
            a = a / 8;
        }
        result = a + result;
        System.out.println("result = " + result);
    }

}
