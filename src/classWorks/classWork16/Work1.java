package classWorks.classWork16;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Work1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//       int i =  returnOne();
        for(int i = 0; i < 3;i++) {
            test2();
        }
//        System.out.println(incrementor(1));

//        System.out.println(returnOne());
    }

    private static void test() throws InterruptedException, ExecutionException {
        CompletableFuture<Void> future = CompletableFuture
                .supplyAsync(Work1::returnOne)
                .thenAccept(i -> {System.out.println("Finished" + i);})
                .thenRun(() -> System.out.println("ALL has been finished"));

        future.get();
        System.out.println("JsonReader finished");
    }

    private static void test1() throws InterruptedException, ExecutionException {
        CompletableFuture<Integer> future = CompletableFuture
                .supplyAsync(Work1::returnOne)
                .thenApply(Work1::incrementor);
//                .thenAccept(i -> {System.out.println("Finished" + i);})
//                .thenRun(() -> System.out.println("ALL has been finished"));

        CompletableFuture<Integer> future2 = future
                .thenCompose(res -> CompletableFuture.supplyAsync(()->res))
                .thenApply(Work1::incrementor);

        System.out.println(future2.get());

//        future.get();
//        System.out.println("JsonReader finished");
    }

    private static void test2() throws InterruptedException, ExecutionException {
        CompletableFuture<Integer> future1 = CompletableFuture
                .supplyAsync(Work1::returnOne)
                .thenApply(Work1::incrementor)
                .thenApply(Work1::incrementor);

        CompletableFuture<Integer> future2 = CompletableFuture
                .supplyAsync(Work1::returnOne)
                .thenApply(Work1::incrementor);

        CompletableFuture<?> future3 = future1.applyToEither(future2, Work1::incrementor);
//                future1
//                .thenCombine(future2, (a,b)->a+b);

        System.out.println(future3.get());

    }


    private static Integer returnOne() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1;
    }

    private static Integer incrementor (Integer i){
        try{
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Value vas incremented on 1");
        return ++i;
    }
}
