package classWorks.Rectangle;

public class MainClass {
    public static void main(String[] args) {
        Point [] points = init();

        show(points);
    }

    private static Point[] init(){
        Point [] mas = new Point[8];
        mas[0] = new Point(10, 1);
        mas[1] = new Point(11, 2);
        mas[2] = new Point(14, 7);
        mas[3] = new Point(14, 9);
        mas[4] = new Point(11, 10);
        mas[5] = new Point(5, 8);
        mas[6] = new Point(4, 3);
        mas[7] = new Point(5, 2);

        return mas;
    }

    private static void show(Point[] mas){
        System.out.println("X: ");
        for(Point point:mas){
            System.out.println(point.getX());
        }

        System.out.println("Y: ");
        for(Point point:mas){
            System.out.println(point.getY());
        }
    }
}
