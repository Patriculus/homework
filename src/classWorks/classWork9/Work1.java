package classWorks.classWork9;

import java.util.ArrayList;
import java.util.List;

public class Work1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10; i++){
            int a = (int)(Math.random() * 100);
            list.add(a);
        }

        System.out.println("Given list is :");
        show(list);

        for (int i = 0; i < list.size() - 1; i++) {
            for(int j =0; j<list.size() -1 -i; j++) {
                if (list.get(j) > list.get(j + 1)) {
                    Integer buff = list.get(j);
                    list.remove(j);

                    list.remove(j+1);
                    list.add(j, list.get(j+1));
                     list.add(j+1, buff);
                    list.remove(j);
                }
            }
        }
        System.out.println("\nSorted list is :");
        show(list);

    }

    private static void show(List<Integer> list) {
        for (Integer i : list){
            System.out.println(i);
        }
    }
}
