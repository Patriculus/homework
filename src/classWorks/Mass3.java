package classWorks;

public class Mass3 {
    public static void main(String[] args) {
        int [] mass = {5, 9, -2, 11};

        int buff = mass[0];
        mass[0] = mass[1];
        mass[1] = buff;

        for(int s:mass){
            System.out.println(s);
        }
    }
}
