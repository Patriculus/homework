package classWorks;

public class ClassWork3 {
    public static void main(String[] args) {

        drawRectangle(3);

    }

    static void drawRectangle(int a, int b, final  int c){
        if(a==0) {
            if(b>1) {
                System.out.println();
                drawRectangle(c, b - 1, c);
            }
            return;
        }

        if(a>0) {
            System.out.print("+");
            drawRectangle(a - 1, b,c);
        }
    }

    static void drawRectangle(int a, int b){
        drawRectangle(a,b,a);
    }

    static void drawRectangle(int a){
        drawRectangle(a, a);
    }
}
