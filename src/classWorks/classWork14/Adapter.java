package classWorks.classWork14;

import java.util.Timer;
import java.util.TimerTask;

public class Adapter {
    private Service service;
    private ICallBack<Integer> callBack;
    private Timer timer;

    public Adapter(ICallBack<Integer> callBack) {
        this.callBack = callBack;
        dataGenerator();
    }

    public void dataGenerator() {
        stopDataGeneration();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                callBack.onDataReceived((int) (Math.random() * 100));
            }
        }, 0, 500);
    }

    public void stopDataGeneration() {
        if (timer == null) {
            return;
        }
        timer.cancel();
        timer = null;
        callBack.onStop();
    }

    public interface ICallBack<T> {
        public void onDataReceived(T data);
        public void onStop();
    }

}
