package classWorks.classWork14;

import java.util.Timer;
import java.util.TimerTask;

public class Service {
    private Adapter adapter;
    private Integer summ = 0;
    private Timer timer;

    public Service() {
        adapter = new Adapter(new Adapter.ICallBack<Integer>() {
            @Override
            public void onDataReceived(Integer data) {
//                System.out.println("Data = " + data);
                summ+=data;
            }

            @Override
            public void onStop() {
                if (timer==null){
                    return;
                }
                timer.cancel();
                timer = null;
            }
        });
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("summ: " + summ);
                summ = 0;
            }
        },3000,3000);
    }

    public  void diconect(){
        adapter.stopDataGeneration();
    }
}
