package classWorks.classWork14;

import java.util.Timer;
import java.util.TimerTask;

public class Main {
    public static void main(String[] args) {
        Service service = new Service();

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                service.diconect();
                timer.cancel();
            }
        }, 15000);
    }
}
