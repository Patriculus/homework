package classWorks.classWork18;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("192.168.89.228", 7030);
//        Socket socket = new Socket("192.168.89.250", 7035);
//

        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());


        BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
        String command = "";

        Thread thread =  new Thread(() -> {
            try{
                while(true){
                    System.out.println("Server ansver " + dataInputStream.readUTF());
                }
            } catch (IOException e) {
                //
            }
        });

        thread.start();

        while(true){
            command = reader.readLine();
            if(command.equals("")){
                continue;
            }



            dataOutputStream.writeUTF(command);
            if(command.equals("stop")){
                dataOutputStream.flush();
                thread.interrupt();
                dataOutputStream.close();
                dataInputStream.close();
                socket.close();
                break;
            }


        }

//        dataOutputStream.writeUTF("Hello1");
//        dataOutputStream.flush();
//        System.out.println(dataInputStream.readUTF());


//        dataOutputStream.close();
//        dataInputStream.close();

//        socket.close();
    }
}
