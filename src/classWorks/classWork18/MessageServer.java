package classWorks.classWork18;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MessageServer extends Thread implements IStoper {
    private ServerSocket sSocket;
    private IStoper stop;

    private boolean isAlive = true;

    public MessageServer(int port) throws IOException {
        sSocket = new ServerSocket(port);
    }

    @Override
    public void run() {
        while (isAlive) {
            System.out.println("Wait for client");
            try {
                Socket socket = sSocket.accept();
                Thread t = new Thread(() ->
                {
                    try {
                        DataInputStream dis = new DataInputStream(socket.getInputStream());
                        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                        String command = "";
                        while (true) {
                            try {
                                command = dis.readUTF();

                                if (command.equals("stop")) {
                                    dos.writeUTF(command);
//                                    dos.flush();
                                    dis.close();
                                    dos.close();
                                    socket.close();
                                    sSocket.close();
                                    stop.stop();
                                    break;
                                }
                                    System.out.println("Client sent to server " + command);
                                    dos.writeUTF(command);
                                    dos.flush();

                            } catch (EOFException e) {
                                dis.close();
                                dos.close();
                                socket.close();
                                break;
                            } catch (IOException e) {
                                System.out.println(e);
                            }


                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                t.setDaemon(true);
                t.start();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public void setStop(IStoper stop) {
        this.stop = stop;
    }

    //    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
//    DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());


}
