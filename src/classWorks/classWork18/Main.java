package classWorks.classWork18;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        MessageServer server = new MessageServer(7030);

        server.start();

        server.setStop(()->{
            server.setAlive(false);
            server.interrupt();
            System.exit(0);
        });
    }
}
