package classWorks.classWork10;

public class Camera extends Product<Camera> {
    private int pixel;

    public void setPixel(int pixel) {
        this.pixel = pixel;
    }

    public int getPixel() {
        return pixel;
    }

    @Override
    public boolean subCompare(Camera camera) {
        return camera.pixel == this.pixel;
    }
}
