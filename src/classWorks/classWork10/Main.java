package classWorks.classWork10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Main {
    public static void main(String[] args) {


//        List<Product> list1 = new ArrayList<>();
//        List<Camera> list2 = new ArrayList<>();
//        List<Object> list3 = new ArrayList<>();
//        list2 = List.of(list1);

        Camera cam1 = new Camera();
        cam1.setPixel(12);
        Camera cam12= new Camera();
        cam12.setPixel(33);

        System.out.println(cam1.subCompare(cam12));

//        copy(list1, list3);
    }


    static void print(Collection<String> list) {
        for (String s : list) {
            System.out.println(s);
        }
    }

    public static void copy(List<? extends Product> src, List<? super Product> dest) {
        for (Product pr : src) {
            dest.add(pr);
        }
    }
}
