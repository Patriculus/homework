package classWorks.classWork10;

public class SellPhone extends Product<SellPhone> {
    private String model;

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    @Override
    public boolean subCompare(SellPhone o) {
        return true;
    }
}
