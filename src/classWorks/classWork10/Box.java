package classWorks.classWork10;

public class Box <T extends Product> {
//public class Box <T extends Product> {
    private T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }
}
