package classWorks.classWork10;

public abstract class Product<T extends Product<T>> implements Comparable<T> {

    private String title;
    private double price;

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int compareTo(T o) {
        return 55;
    }

    public abstract boolean subCompare(T p);
}
