package classWorks;

public class Mass4 {
    public static void main(String[] args) {
        int [] mass = new int[50];

        for(int i = 0; i<mass.length; i++){
            mass[i] = ((i+1)*2)-1;
        }

        for(int r:mass){
            System.out.print(r + " ");
        }
        System.out.println();
        for (int i = mass.length-1; i>=0; i--){
            System.out.print(mass[i] + " ");
        }
    }
}
