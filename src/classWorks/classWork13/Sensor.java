package classWorks.classWork13;

public class Sensor {
    private int id;
    private String data;
    private int type;

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public int getType() {
        return type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Sensor() {
    }

    public Sensor(int id, String data, int type) {
        this.id = id;
        this.data = data;
        this.type = type;
    }
}
