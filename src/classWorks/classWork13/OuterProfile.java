package classWorks.classWork13;

import java.util.List;
import java.util.Set;

public class OuterProfile {
    private String IpProfile;
    private String nicName;
    private int port;
    private String kye;

    private List<Sensor> sensors;
    private Set<Integer> sensorsType;

    public OuterProfile(String ipProfile, String nicName, int port, String kye, List<Sensor> sensors, Set<Integer> sensorsType) {
        IpProfile = ipProfile;
        this.nicName = nicName;
        this.port = port;
        this.kye = kye;
        this.sensors = sensors;
        this.sensorsType = sensorsType;
    }

    public OuterProfile() {
    }

    public String getIpProfile() {
        return IpProfile;
    }

    public String getNicName() {
        return nicName;
    }

    public int getPort(int port) {
        return this.port;
    }

    public String getKye() {
        return kye;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public Set<Integer> getSensorsType() {
        return sensorsType;
    }

    public void setIpProfile(String ipProfile) {
        IpProfile = ipProfile;
    }

    public void setNicName(String nicName) {
        this.nicName = nicName;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setKye(String kye) {
        this.kye = kye;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public void setSensorsType(Set<Integer> sensorsType) {
        this.sensorsType = sensorsType;
    }
}
