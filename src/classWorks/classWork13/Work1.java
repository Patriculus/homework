/*
package classWorks.classWork13;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.*;

public class Main {
    static Map<String, OuterProfile> map = new HashMap<>();
    private static final String ROOT = "File.bin";

    public static void main(String[] args) throws IOException {
        putValues(map);
        String data = new Gson().toJson(map);
//        createAndWriteFile(data);

        Map<String, OuterProfile> resporedMap;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(OuterProfile.class, new OuterProfileDeserializer());
        Gson gson = gsonBuilder.create();

        Type type = new TypeToken<Map<String, OuterProfile>>(){}.getType();
        resporedMap = gson.fromJson(data, type);
        System.out.println(data);
    }

    public static void createAndWriteFile(String s) throws IOException {
        File file = new File(ROOT);

        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        outputStream.flush();
        outputStream.close();
    }

    public static String readFile() throws IOException {
        File file = new File(ROOT);
        if(!file.exists()){
            return null;
        }

        FileInputStream inputStream  = new FileInputStream(file);
        byte [] sss = new byte[inputStream.readAllBytes().length];
        inputStream.read(sss);
        String o = new String(sss);
        inputStream.close();
        return o;

    }

    static void putValues(Map<String, OuterProfile> mapToPut) {
        String uuid = UUID.randomUUID().toString();
        Sensor sensor1 = new Sensor(1, "GPS", 10);
        Sensor sensor2 = new Sensor(2, "Meteo", 20);
        Sensor sensor3 = new Sensor(3, "RangeFinder", 30);

        List<Sensor> sensors = new ArrayList<>();
        sensors.add(sensor1);
        sensors.add(sensor3);
        List<Sensor> sensors1 = new ArrayList<>();
        sensors1.add(sensor2);
        sensors1.add(null);

        Set<Integer> sensorsType = new HashSet<>();

        OuterProfile outerProfile = new OuterProfile("192.168.1.1", "Patryck", 361, uuid, sensors, setSet(sensors));
        map.put(uuid, outerProfile);

        uuid = UUID.randomUUID().toString();
        OuterProfile outerProfile1 = new OuterProfile("192.168.1.1", "Patryck", 361, uuid, sensors1, setSet(sensors1));
        map.put(uuid, outerProfile1);
    }

    private static Set<Integer> setSet(List<Sensor> sensors) {
        Set<Integer> set = new HashSet<>();
        for (Sensor sensor : sensors) {
            if(sensor == null) continue;
            set.add(sensor.getType());
        }
        return set;
    }

}
*/
