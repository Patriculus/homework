/*
package classWorks.classWork13;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

public class OuterProfileDeserializer implements JsonDeserializer<OuterProfile> {
    @Override
    public OuterProfile deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

        OuterProfile profile = new OuterProfile();

        String IpProfile;
        String nicName;
        int port;
        String kye;

        List<Sensor> sensors;
        Set<Integer> sensorsType;
        JsonObject jsonObject = jsonElement.getAsJsonObject();


        IpProfile= jsonObject.get("IpProfile").getAsString();
        nicName= jsonObject.get("nicName").getAsString();
        port= jsonObject.get("port").getAsInt();
        kye= jsonObject.get("kye").getAsString();



        GsonBuilder gsonBuilder = new GsonBuilder();
        Type t = new TypeToken<List<Sensor>>(){}.getType();
        gsonBuilder.registerTypeAdapter(t, new SensorListDeserializer());
        Gson gson = gsonBuilder.create();
        sensors = gson.fromJson(jsonObject.get("sensors"), t);

        profile.setIpProfile(IpProfile);
        profile.setKye(kye);
        profile.setNicName(nicName);
        profile.getPort(port);

        return profile;
    }
}
*/
