//package classWorks.classWork13;
//
//import com.google.gson.*;
//
//import java.lang.reflect.Type;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//
//public class SensorListDeserializer  implements JsonDeserializer<List<Sensor>> {
//    @Override
//    public List<Sensor> deserialize(JsonParseExceptionlement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
//        List<Sensor> sensors = new ArrayList<>();
//        JsonArray jsonArray = jsonElement.getAsJsonArray();
//        Iterator<JsonElement> iter = jsonArray.iterator();
//        JsonElement element;
//        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(Sensor.class, new SensorDeserializer());
//        Gson gson = gsonBuilder.create();
//
//        while(iter.hasNext()){
//            element = iter.next();
//            sensors.add(gson.fromJson(element, Sensor.class));
//        }
//
//        return sensors;
//    }
//}
