package dev.homeWorks.homeWork1;

import java.io.*;
import java.lang.reflect.Field;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


public class Server {
    private static ArrayList<String> filesNames;
    private static File folder;

    //    List<String> filesNames;
    public static void main(String[] args) throws IOException {
        filesNames = new ArrayList<>();
        folder = new File("D:\\Patryck\\src");
        File picture = new File("D:\\Patryck\\Face.jpg");

        getFiles(folder);
        System.out.println("Server send to client :" + filesNames);


        ServerSocket sSocket = new ServerSocket(7035);
        Socket socket = sSocket.accept();

        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());

        System.out.println("Server started");

        String command = "";
        while (true) {
            try {
//                System.out.println("Waiting for commmand");
                command = dataInputStream.readUTF();
                System.out.println("Text from client is :" + command);

//                System.out.println("Server send to client :" + command );
//                dataOutputStream.writeUTF(command);
//                dataOutputStream.flush();


                switch (command) {
                    case "getDate": {
                        String date = new Date().toString();
                        dataOutputStream.writeUTF(date);
                        dataOutputStream.flush();
                        System.out.println("Server send to client :" + date);
                        break;
                    }
                    case "getFilesList": {
                        getFiles(folder);
                        StringBuilder answer = new StringBuilder();

                        for(String name:filesNames){
                            answer.append(name).append("\n");
                        }

                        dataOutputStream.writeUTF(answer.toString());
                        dataOutputStream.flush();
                        System.out.println("Server send to client :" + filesNames);
                        break;
                    }
                    case "getSize":{
                        dataOutputStream.writeInt((int) picture.length());
                        dataOutputStream.flush();
                        System.out.println("Server send to client :" + picture.length() + " bytes");
                        break;
                    }
                    case "getPhoto":{

                        dataOutputStream.writeInt((int) picture.length());
                        dataOutputStream.flush();
                        System.out.println("Server send to client :" + picture.length() + " bytes");

                        byte [] fileSize = new byte[(int) picture.length()];
                        FileInputStream fis = null;
                        BufferedInputStream bis = null;

                        fis = new FileInputStream(picture);
                        bis = new BufferedInputStream(fis);

                        bis.read(fileSize,0,fileSize.length);

                        System.out.println("Sending " + "(" + fileSize.length + " bytes)");
                        dataOutputStream.write(fileSize,0,fileSize.length);
                        dataOutputStream.flush();
                        System.out.println("Done.");
                    }
                    default:break;

                }

            } catch (Exception e) {

            }

        }
//        dataOutputStream.close();
//        dataInputStream.close();
//
//        socket.close();
    }


    public static void getFiles(File file) {


        for (File f : Objects.requireNonNull(file.listFiles())) {
            if (f.isDirectory()) {
                getFiles(f);
            } else {
                filesNames.add(f.getName().split("\\.")[0]);
            }
        }
    }
}
