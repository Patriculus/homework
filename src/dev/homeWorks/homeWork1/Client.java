package dev.homeWorks.homeWork1;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Paths;

public class Client {
    public static void main(String args[]) throws Exception {

//        System.out.println(System.getProperty("user.dir"));
//
//        // Java 7
//        System.out.println(Paths.get("").toAbsolutePath().toString());

        Socket socket = new Socket("192.168.1.208", 7035);
//        Socket socket = new Socket(InetAddress.getLocalHost(), 7035);
//        Socket socket = new Socket("31.43.132.107", 7035);

        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

        String text = "data";
//        String text = "getDate";
        System.out.println("Client send " + text);
        out.writeUTF(text);
        out.flush();
        System.out.println("Server ansver " + in.readUTF());

        text = "gphoto";
//        text = "getFilesList";
        System.out.println("Client send " + text);
        out.writeUTF(text);
        out.flush();
        System.out.println("Server ansver " + in.readUTF());


        text = "getSize";
        System.out.println("Client send " + text);
        out.writeUTF(text);
        out.flush();
        int fileSize = in.readInt();
        System.out.println("Server ansver " + fileSize);

        text = "getPhoto";
        System.out.println("Client send " + text);
        out.writeUTF(text);
        out.flush();

        byte[] bytes = new byte[fileSize];

        in.read(bytes);

//        for(byte b:bytes){
////            System.out.print((char)b);
////        }
        File file = new File("D:\\Patryck\\Morda.jpg");
        if (!file.exists()) file.createNewFile();

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);

//        fos.close();
//        out.close();
//        in.close();
//        socket.close();
    }
}
